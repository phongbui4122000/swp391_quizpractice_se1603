package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class Register_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!DOCTYPE html>\r\n");
      out.write("\r\n");
      out.write("<html lang=\"en\"\r\n");
      out.write("      dir=\"ltr\">\r\n");
      out.write("\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta charset=\"utf-8\">\r\n");
      out.write("        <meta http-equiv=\"X-UA-Compatible\"\r\n");
      out.write("              content=\"IE=edge\">\r\n");
      out.write("        <meta name=\"viewport\"\r\n");
      out.write("              content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\r\n");
      out.write("        <title>Signup</title>\r\n");
      out.write("\r\n");
      out.write("        <!-- Custom Fonts -->\r\n");
      out.write("        <link href=\"https://fonts.googleapis.com/css?family=Oswald:400,500,700%7CRoboto:400,500%7CRoboto:400,500&display=swap\"\r\n");
      out.write("              rel=\"stylesheet\">\r\n");
      out.write("\r\n");
      out.write("        <!-- Perfect Scrollbar -->\r\n");
      out.write("        <link type=\"text/css\"\r\n");
      out.write("              href=\"assets/vendor/perfect-scrollbar.css\"\r\n");
      out.write("              rel=\"stylesheet\">\r\n");
      out.write("\r\n");
      out.write("        <!-- Material Design Icons -->\r\n");
      out.write("        <link type=\"text/css\"\r\n");
      out.write("              href=\"assets/css/material-icons.css\"\r\n");
      out.write("              rel=\"stylesheet\">\r\n");
      out.write("\r\n");
      out.write("        <!-- Font Awesome Icons -->\r\n");
      out.write("        <link type=\"text/css\"\r\n");
      out.write("              href=\"assets/css/fontawesome.css\"\r\n");
      out.write("              rel=\"stylesheet\">\r\n");
      out.write("\r\n");
      out.write("        <!-- Preloader -->\r\n");
      out.write("        <link type=\"text/css\"\r\n");
      out.write("              href=\"assets/vendor/spinkit.css\"\r\n");
      out.write("              rel=\"stylesheet\">\r\n");
      out.write("\r\n");
      out.write("        <!-- App CSS -->\r\n");
      out.write("        <link type=\"text/css\"\r\n");
      out.write("              href=\"assets/css/app.css\"\r\n");
      out.write("              rel=\"stylesheet\">\r\n");
      out.write("\r\n");
      out.write("    </head>\r\n");
      out.write("\r\n");
      out.write("    <body class=\"login\">\r\n");
      out.write("\r\n");
      out.write("        <div class=\"d-flex align-items-center\"\r\n");
      out.write("             style=\"min-height: 100vh\">\r\n");
      out.write("            <div class=\"col-sm-8 col-md-6 col-lg-4 mx-auto\"\r\n");
      out.write("                 style=\"min-width: 300px;\">\r\n");
      out.write("                <div class=\"d-flex justify-content-center mb-5 navbar-light\">\r\n");
      out.write("                    <!-- Brand -->\r\n");
      out.write("                    <a href=\"Home\"\r\n");
      out.write("                       class=\"navbar-brand m-0\">\r\n");
      out.write("                        QuizPractice\r\n");
      out.write("                    </a>\r\n");
      out.write("                </div>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("                <div class=\"card navbar-shadow\">\r\n");
      out.write("                    <div class=\"card-header text-center\">\r\n");
      out.write("                        <h4 class=\"card-title\">Sign Up</h4>\r\n");
      out.write("                        <p class=\"card-subtitle\">Create a new account</p>\r\n");
      out.write("                    </div>\r\n");
      out.write("\r\n");
      out.write("                    <!--Body-->\r\n");
      out.write("                    <div class=\"card-body\">\r\n");
      out.write("                        <form action=\"VerifyUser\"\r\n");
      out.write("                              method=\"post\">\r\n");
      out.write("\r\n");
      out.write("                            <!--Name-->\r\n");
      out.write("                            <div class=\"form-group\">\r\n");
      out.write("                                <label class=\"form-label\"\r\n");
      out.write("                                       for=\"name\">Name:</label>\r\n");
      out.write("                                <div class=\"input-group input-group-merge\">\r\n");
      out.write("                                    <input id=\"name\"\r\n");
      out.write("                                           type=\"text\"\r\n");
      out.write("                                           required\r\n");
      out.write("                                           name=\"user_name\"\r\n");
      out.write("                                           class=\"form-control form-control-prepended\"\r\n");
      out.write("                                           placeholder=\"Your first and last name\">\r\n");
      out.write("                                    <div class=\"input-group-prepend\">\r\n");
      out.write("                                        <div class=\"input-group-text\">\r\n");
      out.write("                                            <span class=\"far fa-user\"></span>\r\n");
      out.write("                                        </div>\r\n");
      out.write("                                    </div>\r\n");
      out.write("                                </div>\r\n");
      out.write("                            </div>\r\n");
      out.write("\r\n");
      out.write("                            <!--Email-->\r\n");
      out.write("                            <div class=\"form-group\">\r\n");
      out.write("                                <label class=\"form-label\"\r\n");
      out.write("                                       for=\"email\">Email address:</label>\r\n");
      out.write("                                <div class=\"input-group input-group-merge\">\r\n");
      out.write("                                    <input id=\"email\"\r\n");
      out.write("                                           name=\"email\"\r\n");
      out.write("                                           type=\"email\"\r\n");
      out.write("                                           required\r\n");
      out.write("                                           class=\"form-control form-control-prepended\"\r\n");
      out.write("                                           placeholder=\"Your email address\">\r\n");
      out.write("                                    <div class=\"input-group-prepend\">\r\n");
      out.write("                                        <div class=\"input-group-text\">\r\n");
      out.write("                                            <span class=\"far fa-envelope\"></span>\r\n");
      out.write("                                        </div>\r\n");
      out.write("                                    </div>\r\n");
      out.write("                                </div>\r\n");
      out.write("                            </div>\r\n");
      out.write("\r\n");
      out.write("                            <!--Password-->\r\n");
      out.write("                            <div class=\"form-group\">\r\n");
      out.write("                                <label class=\"form-label\"\r\n");
      out.write("                                       for=\"password\">Password:</label>\r\n");
      out.write("                                <div class=\"input-group input-group-merge\">\r\n");
      out.write("                                    <input id=\"password\"\r\n");
      out.write("                                           type=\"password\"\r\n");
      out.write("                                           required\r\n");
      out.write("                                           name=\"password\"\r\n");
      out.write("                                           class=\"form-control form-control-prepended\"\r\n");
      out.write("                                           placeholder=\"Choose a password\"\r\n");
      out.write("                                           minlength=\"6\"\r\n");
      out.write("                                           maxlength=\"32\">\r\n");
      out.write("                                    <div class=\"input-group-prepend\">\r\n");
      out.write("                                        <div class=\"input-group-text\">\r\n");
      out.write("                                            <span class=\"far fa-key\"></span>\r\n");
      out.write("                                        </div>\r\n");
      out.write("                                    </div>\r\n");
      out.write("                                </div>\r\n");
      out.write("                            </div>\r\n");
      out.write("\r\n");
      out.write("                            <!--Confirm-->\r\n");
      out.write("                            <div class=\"form-group\">\r\n");
      out.write("                                <label class=\"form-label\"\r\n");
      out.write("                                       for=\"password2\">Re-Enter Password:</label>\r\n");
      out.write("                                <div class=\"input-group input-group-merge\">\r\n");
      out.write("                                    <input id=\"password2\"\r\n");
      out.write("                                           type=\"password\"\r\n");
      out.write("                                           required\r\n");
      out.write("                                           name=\"re_password\"\r\n");
      out.write("                                           class=\"form-control form-control-prepended\"\r\n");
      out.write("                                           placeholder=\"Confirm password\"\r\n");
      out.write("                                           minlength=\"6\"\r\n");
      out.write("                                           maxlength=\"32\">\r\n");
      out.write("                                    <div class=\"input-group-prepend\">\r\n");
      out.write("                                        <div class=\"input-group-text\">\r\n");
      out.write("                                            <span class=\"far fa-key\"></span>\r\n");
      out.write("                                        </div>\r\n");
      out.write("                                    </div>\r\n");
      out.write("                                </div>\r\n");
      out.write("                            </div>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("                            <div style=\"color: red\">");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.message1}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</div>\r\n");
      out.write("                            <div style=\"color: red\">");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.message2}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</div>\r\n");
      out.write("                            <br/>\r\n");
      out.write("                            <!--Submit-->\r\n");
      out.write("                              <div class=\"form-group text-center mb-0\">\r\n");
      out.write("                                <div class=\"custom-control custom-checkbox\">\r\n");
      out.write("                                    <input id=\"terms\"\r\n");
      out.write("                                           type=\"checkbox\"\r\n");
      out.write("                                           class=\"custom-control-input\"\r\n");
      out.write("                                           checked\r\n");
      out.write("                                           required value=\"submit\" onclick=\"if(!this.form.checkbox.checked){alert('You must agree to the terms first.');return false}\">\r\n");
      out.write("                                    <label for=\"terms\"\r\n");
      out.write("                                           class=\"custom-control-label text-black-70\">I agree to the <a data-toggle=\"modal\" href=\"#modal\">Terms of Use</a></label>                         \r\n");
      out.write("                                </div>\r\n");
      out.write("                            </div><br/>\r\n");
      out.write("                            <button type=\"submit\"\r\n");
      out.write("                                    class=\"btn btn-primary btn-block mb-3\">Sign Up</button>\r\n");
      out.write("                            \r\n");
      out.write("                        </form>\r\n");
      out.write("                    </div>\r\n");
      out.write("                    <div class=\"card-footer text-center text-black-50\">Already signed up? <a href=\"Login.jsp\">Login</a></div>\r\n");
      out.write("                </div>\r\n");
      out.write("            </div>\r\n");
      out.write("        </div>\r\n");
      out.write("                            \r\n");
      out.write("                              \r\n");
      out.write("\r\n");
      out.write("        <!-- jQuery -->\r\n");
      out.write("        <script src=\"assets/vendor/jquery.min.js\"></script>\r\n");
      out.write("\r\n");
      out.write("        <!-- Bootstrap -->\r\n");
      out.write("        <script src=\"assets/vendor/popper.min.js\"></script>\r\n");
      out.write("        <script src=\"assets/vendor/bootstrap.min.js\"></script>\r\n");
      out.write("\r\n");
      out.write("        <!-- Perfect Scrollbar -->\r\n");
      out.write("        <script src=\"assets/vendor/perfect-scrollbar.min.js\"></script>\r\n");
      out.write("\r\n");
      out.write("        <!-- MDK -->\r\n");
      out.write("        <script src=\"assets/vendor/dom-factory.js\"></script>\r\n");
      out.write("        <script src=\"assets/vendor/material-design-kit.js\"></script>\r\n");
      out.write("\r\n");
      out.write("        <!-- App JS -->\r\n");
      out.write("        <script src=\"assets/js/app.js\"></script>\r\n");
      out.write("\r\n");
      out.write("        <!-- Highlight.js -->\r\n");
      out.write("        <script src=\"assets/js/hljs.js\"></script>\r\n");
      out.write("\r\n");
      out.write("        <!-- App Settings (safe to remove) -->\r\n");
      out.write("        <script src=\"assets/js/app-settings.js\"></script>\r\n");
      out.write("\r\n");
      out.write("    </body>\r\n");
      out.write("\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
